#!/bin/bash
docker-compose down
cd rpg-backend/
mvn package
cd ..
docker build -t rpg-backend rpg-backend
docker build -t rpg-frontend rpg-frontend
docker-compose up