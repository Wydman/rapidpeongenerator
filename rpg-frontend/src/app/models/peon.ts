export class Peon {
   name: string;
   lastName: string;
   sex: string;
   attributes: string;

  constructor(name: string, lastName: string, sex: string, attributes: string) {
    this.name = name;
    this.lastName = lastName;
    this.sex = sex;
    this.attributes = attributes;

  }
}
