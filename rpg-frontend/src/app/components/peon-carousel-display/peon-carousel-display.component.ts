import {Component, Input, OnInit} from '@angular/core';
import {Peon} from '../../models/peon';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {PeonsService} from '../../service/peons.service';

@Component({
  selector: 'app-peon-carousel-display',
  templateUrl: './peon-carousel-display.component.html',
  styleUrls: ['./peon-carousel-display.component.css']
})
export class PeonCarouselDisplayComponent implements OnInit {

 @Input() peonList: Peon[];

  constructor(private peonsService: PeonsService) {
  }

  ngOnInit() {
    this.peonList = this.peonsService.requestedPeonList;
    console.log(this.peonList);

  }

}
