import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeonCarouselDisplayComponent } from './peon-carousel-display.component';

describe('PeonCarouselDisplayComponent', () => {
  let component: PeonCarouselDisplayComponent;
  let fixture: ComponentFixture<PeonCarouselDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeonCarouselDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeonCarouselDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
