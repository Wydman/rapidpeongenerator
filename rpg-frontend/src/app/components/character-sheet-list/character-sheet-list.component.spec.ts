import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSheetListComponent } from './character-sheet-list.component';

describe('CharacterSheetListComponent', () => {
  let component: CharacterSheetListComponent;
  let fixture: ComponentFixture<CharacterSheetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSheetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSheetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
