import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PeonsService} from '../../service/peons.service';
import {Peon} from '../../models/peon';

@Component({
  selector: 'app-character-sheet-list',
  templateUrl: './character-sheet-list.component.html',
  styleUrls: ['./character-sheet-list.component.css']
})
export class CharacterSheetListComponent implements OnInit {

  peonList:Peon[];

  constructor(private http: HttpClient,private service: PeonsService) { }

  ngOnInit() {
    this.service.getAllStudents()
      .subscribe(subscribedPeonList => this.peonList = subscribedPeonList)
  }

}




