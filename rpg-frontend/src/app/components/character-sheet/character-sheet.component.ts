import {Component, Input, OnInit} from '@angular/core';
import {Peon} from '../../models/peon';

@Component({
  selector: 'app-character-sheet',
  templateUrl: './character-sheet.component.html',
  styleUrls: ['./character-sheet.component.css']
})
export class CharacterSheetComponent implements OnInit {

  @Input() peon: Peon;

  constructor() { }

  ngOnInit() {
  }

}
