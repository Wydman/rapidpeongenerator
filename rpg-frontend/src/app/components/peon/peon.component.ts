import {Component, Input, OnInit} from '@angular/core';
import {Peon} from '../../models/peon';

@Component({
  selector: 'app-peon',
  templateUrl: './peon.component.html',
  styleUrls: ['./peon.component.css']
})
export class PeonComponent implements OnInit {

  @Input() peon: Peon;

  constructor() { }

  ngOnInit() {
  }

}
