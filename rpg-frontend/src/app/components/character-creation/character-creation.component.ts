import {Component, Input, OnInit, Output} from '@angular/core';
import {PeonsService} from '../../service/peons.service';
import {Peon} from '../../models/peon';
import {Router} from '@angular/router';

@Component({
  selector: 'app-character-creation',
  templateUrl: './character-creation.component.html',
  styleUrls: ['./character-creation.component.css']
})
export class CharacterCreationComponent implements OnInit {

  maxPointAvailable = 300;
  maxPointPerStat = 75;
  total = 0;
  numberOfPeons = 1;
  entropyLevel = 50;
  @Input() strength = 0;
  @Input() intelligence = 0;
  @Input() agility = 0;
  @Input() perception = 0;
  @Input() charisma = 0;
  @Input() luck = 0;

  @Input() isReadytoSubmit = true;
  @Input() submitted = false;

  createdPeonsList: Peon[];

  constructor(private peonsService: PeonsService,private router:Router) {

  }

  ngOnInit() {
  }

  onSubmit(form: any): void {
    console.log('you submitted value:', form);
    this.peonsService.createNewPeon(form).subscribe(peonList => {
      console.log('you received value:', peonList);
      this.peonsService.requestedPeonList = peonList;
      this.createdPeonsList = peonList;
      this.submitted = true;
      this.router.navigate(['/characters-carousel-display']);
    });
  }

  updateAvailablePoints(attribute: number) {
    this.total = this.strength + this.intelligence + this.agility + this.perception + this.charisma + this.luck;
    this.maxPointAvailable = 300 - this.total;
    if (attribute > 75 || attribute < 0) {
      this.isReadytoSubmit = false;
    } else {
      this.isReadytoSubmit = true;
    }
  }
}
