import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {PeonsService} from './service/peons.service';
import { PeonComponent } from './components/peon/peon.component';
import { CharacterSheetComponent } from './components/character-sheet/character-sheet.component';
import { CharacterSheetListComponent } from './components/character-sheet-list/character-sheet-list.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {RouterModule, Routes} from '@angular/router';
import { CharacterCreationComponent } from './components/character-creation/character-creation.component';
import {FormsModule} from '@angular/forms';
import { PeonCarouselDisplayComponent } from './components/peon-carousel-display/peon-carousel-display.component';

const  appRoutes:Routes =[
  { path: 'characters-sheets', component:CharacterSheetListComponent },
  { path: 'characters-creation', component:CharacterCreationComponent },
  { path: 'characters-carousel-display', component:PeonCarouselDisplayComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    PeonComponent,
    CharacterSheetComponent,
    CharacterSheetListComponent,
    InventoryComponent,
    HeaderComponent,
    FooterComponent,
    CharacterCreationComponent,
    PeonCarouselDisplayComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
  ],
  providers: [PeonsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
