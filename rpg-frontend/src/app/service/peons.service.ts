import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Peon} from '../models/peon';

@Injectable()
export class PeonsService {

  public requestedPeonList:Peon[];

  constructor(private Http: HttpClient) {
  }

  getAllStudents(): Observable<Peon[]> {
    return this.Http.get<Peon[]>('http://localhost:8080/peon');
  }

  createNewPeon(peonJson:JSON): Observable<Peon[]> {
    console.log(peonJson);
    return this.Http.post<Peon[]>('http://localhost:8080/peon/generator', peonJson);
  }
}
