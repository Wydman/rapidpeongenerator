package com.wydman.rpgbackend.Repository;

import javax.persistence.Id;
import java.util.stream.Stream;

public class Attributes {

    private String peonID;
    private int dexterity;
    private int perception;
    private int intelligence;
    private int strength;
    private int agility;
    private int luck;

    public Attributes(String peonID, int dexterity, int perception,
                      int intelligence, int strength,
                      int agility, int luck) {
        this.peonID = peonID;
        this.dexterity = dexterity;
        this.perception = perception;
        this.intelligence = intelligence;
        this.strength = strength;
        this.agility = agility;
        this.luck = luck;
    }

    public String toString() {
        return this.dexterity + " " + this.perception + " " + this.intelligence + " " + this.strength + " " + this.agility + " " + this.luck;
    }
}
