package com.wydman.rpgbackend.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface PeonRepository extends CrudRepository<Peon,String> {


}
