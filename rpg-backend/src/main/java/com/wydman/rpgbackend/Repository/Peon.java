package com.wydman.rpgbackend.Repository;

import javax.persistence.*;

@Entity
public class Peon {

    @Id
    private String id;
    private String name;
    private String lastName;
    private String sex;
    private String race;
    private String backgroud;
    private String attributes;

    public Peon(String id, String name, String lastName, String sex, String race, String backgroud, String attributes) {

        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.sex = sex;
        this.race = race;
        this.backgroud = backgroud;
        this.attributes = attributes;
    }

    public Peon() {

    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getBackgroud() {
        return backgroud;
    }

    public void setBackgroud(String backgroud) {
        this.backgroud = backgroud;
    }
}
