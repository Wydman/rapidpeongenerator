package com.wydman.rpgbackend.Domain;

import com.wydman.rpgbackend.Controllers.PeonRepresentation;
import com.wydman.rpgbackend.Repository.Peon;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PeonRepresentationFactory {

    public List<PeonRepresentation> getPeonRepresentationsFromPeons(List<Peon> peonList) {
        List<PeonRepresentation> peonRepresentationList = new ArrayList<>();

        for (Peon peon : peonList) {
            peonRepresentationList.add(buildPeonRepresentation(peon));
        }
        return peonRepresentationList;
    }

    private PeonRepresentation buildPeonRepresentation(Peon peon) {
        return new PeonRepresentation(peon.getName(), peon.getLastName(), peon.getSex(), peon.getRace(), peon.getBackgroud(),peon.getAttributes());
    }
}
