package com.wydman.rpgbackend.Domain;
import com.wydman.rpgbackend.Repository.Attributes;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AttributesGenerator {

    //TODO fixer le fait que la variation est trop forte quand un utilisateur choisi des attributs de base, même si le treshold est très bas !

    public Attributes generateAttributes(String peonId, int entropyLevel, List<Integer> incomingAttributeList) {
        Random rand = new Random();
        int remainingPoints = 300;

        HashMap<Integer, Integer> attributesMap = new HashMap<>();

        int thresholdToSpend = 0;
        int numberOfStatsToGen = 0;

        List<Integer> valueToAddInMap = new ArrayList<>();

        for (int i = 0; i < incomingAttributeList.size(); i++) {
            if (incomingAttributeList.get(i) == 0) {
                numberOfStatsToGen++;
                valueToAddInMap.add(i);
            } else {
                remainingPoints -= incomingAttributeList.get(i);
            }
            attributesMap.put(i, incomingAttributeList.get(i));
        }

        if (entropyLevel <= 0) {
            entropyLevel = 1;
        }

        for (int i : valueToAddInMap) {
            int value = Math.abs(remainingPoints / numberOfStatsToGen + (thresholdToSpend + rand.nextInt(entropyLevel - (-entropyLevel)) + (-entropyLevel)));
            if ((remainingPoints - value) > 0 && value < 75) {
                attributesMap.put(i, value);
                remainingPoints -= value;
            }
        }

        int keyToIgnore = rand.nextInt(valueToAddInMap.size());

        while (remainingPoints > 0) {

            for (int i : valueToAddInMap) {

                if (i == keyToIgnore && keyToIgnore != 0 &&
                        remainingPoints - 2 > 0 && attributesMap.get(i) < 75 && entropyLevel > 10) {
                    attributesMap.put(i, attributesMap.get(i) + 2);
                    remainingPoints -= 2;
                } else if (rand.nextBoolean() && i == keyToIgnore && attributesMap.get(i) < 75) {
                    attributesMap.put(i, attributesMap.get(i) + 1);
                    remainingPoints--;
                } else if (rand.nextBoolean() && remainingPoints > 0 && attributesMap.get(i) < 75) {
                    attributesMap.put(i, attributesMap.get(i) + 1);
                    remainingPoints--;
                }
            }
        }

        if (valueToAddInMap.size() == 6 && entropyLevel < 10) {
            List<Integer> keys = new ArrayList(attributesMap.values());
            Collections.shuffle(keys);
            int index = 0;
            for (Integer i : keys) {

                attributesMap.put(index++, i);
            }
        }
        return new Attributes(peonId, attributesMap.get(0), attributesMap.get(1), attributesMap.get(2),
                attributesMap.get(3), attributesMap.get(4), attributesMap.get(5));
    }
}
