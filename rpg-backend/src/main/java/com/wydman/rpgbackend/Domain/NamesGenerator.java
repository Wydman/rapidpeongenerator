package com.wydman.rpgbackend.Domain;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
public class NamesGenerator {
    Random rand = new Random();

    private List<String> firstPart = List.of("Co", "Pu", "La", "Tu", "Te", "Fli", "Plo",
            "Fa", "Ri", "Er", "Cha", "Pi", "Ma", "Ste", "Me", "Ga", "Ba", "Sa", "Cy", "Aur", "Jo", "Nu",
            "Pli", "Ta", "So", "Lo", "Ra", "Cho", "Le", "La", "Ru", "Kla", "Fu", "Fo", "Re", "Se", "Si",
            "Ka", "Che", "Bo", "Pe", "Be", "Hi", "Je", "No", "Vlu", "Arn", "Cu", "Vi", "Ca", "Xia", "Shue",
            "Ti", "Te", "To", "Da", "Pa", "Glon", "Ja", "Ji", "Do", "Mi", "Mu", "Mo", "Va", "Ve", "Vy");

    private List<String> secondPart = List.of("co", "pu", "la", "tu", "te", "fli", "plo",
            "fa", "ri", "er", "cha", "pi", "ma", "ste", "me", "ga", "ba", "sa", "cy", "aur", "jo", "nu",
            "pli", "ta", "so", "lo", "ra", "cho", "le", "la", "ru", "kla", "fu", "fo", "re", "se", "si",
            "ka", "che", "bo", "pe", "be", "hi", "je", "no", "vlu", "arn", "cu", "vi", "ca", "xia", "shue",
            "ti", "te", "to", "da", "pa", "glon", "ja", "ji", "do", "mi", "mu", "mo", "va", "ve", "vy");

    private List<String> thirdPart = List.of("aud", "l", "u", "tu", "te", "fli", "plo",
            "fa", "e", "r", "cha", "pi", "ma", "ste", "ss", "Gga", "ba", "q", "cy", "ie", "Jo", "nu",
            "pli", "ta", "so", "lo", "l", "sse", "h", "la", "ru", "kla", "e", "fo", "Re", "se", "si");

    public String generateName() {
        return firstPart.get(rand.nextInt(firstPart.size())) + secondPart.get(rand.nextInt(secondPart.size())) + thirdPart.get(rand.nextInt(thirdPart.size()));
    }

    public String generateLastName() {
        return firstPart.get(rand.nextInt(firstPart.size())) + secondPart.get(rand.nextInt(secondPart.size())) + thirdPart.get(rand.nextInt(thirdPart.size()));
    }
}
