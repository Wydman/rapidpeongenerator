package com.wydman.rpgbackend.Domain;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UuidGenerator {
    public String generatePeonId() {
        return UUID.randomUUID().toString();
    }
}
