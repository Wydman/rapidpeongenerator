package com.wydman.rpgbackend.Domain;


import com.wydman.rpgbackend.Service.RapidPeonGeneratorCore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration
public class UtilsBeans {

    @Bean
    public CommandLineRunner commandLineRunner(RapidPeonGeneratorCore rapidPeonGeneratorCore) {
        return args -> {
            rapidPeonGeneratorCore.deleteAll();
            rapidPeonGeneratorCore.start();
        };
    }

    @Bean
    public Scanner createScanner() {
        return new Scanner(System.in);
    }

    @Bean
    public Logger logger() {
        return LoggerFactory.getLogger("QuizLogger");
    }
}
