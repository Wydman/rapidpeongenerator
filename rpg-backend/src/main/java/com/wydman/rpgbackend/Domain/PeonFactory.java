package com.wydman.rpgbackend.Domain;

import com.wydman.rpgbackend.Controllers.PeonCreationRequestRepresentation;
import com.wydman.rpgbackend.Controllers.PeonRepresentation;
import com.wydman.rpgbackend.Repository.Peon;
import com.wydman.rpgbackend.Repository.PeonRepository;
import org.hibernate.id.UUIDGenerator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class PeonFactory {

    private PeonRepository peonRepository;
    private AttributesGenerator attributesGenerator;
    private NamesGenerator namesGenerator;
    private Random random;
    private UuidGenerator uuidGenerator;

    PeonFactory(PeonRepository peonRepository, AttributesGenerator attributesGenerator,
                UuidGenerator uuidGenerator, NamesGenerator namesGenerator) {

        this.peonRepository = peonRepository;
        this.attributesGenerator = attributesGenerator;
        this.namesGenerator = namesGenerator;
        this.random = new Random();
        this.uuidGenerator = uuidGenerator;
    }


    public List<Peon> CreatePeon(PeonCreationRequestRepresentation peonCreationRequestRepresentation) {
        ArrayList<Peon> peonslistToReturn = new ArrayList<>();
        int numberOfPeonsToCreate = peonCreationRequestRepresentation.getNumberOfPeonToCreate();

        boolean isNamed = false, hasLastName = false, hasGender = false, hasRace = false, hasBackground = false;
        String id = "", attributes = "", name = "", lastName = "", gender = "", race = "", background = "";

        if (peonCreationRequestRepresentation.getName() != null) {
            isNamed = true;
        }
        if (peonCreationRequestRepresentation.getLastName() != null) {
            hasLastName = true;
        }
        if (peonCreationRequestRepresentation.getSex() != null) {
            hasGender = true;
        }
        if (peonCreationRequestRepresentation.getRace() != null) {
            hasRace = true;
        }
        if (!peonCreationRequestRepresentation.getBackground().equals("Like to eat pies")) {
            hasBackground = true;
        }

        for (int i = 0; i < numberOfPeonsToCreate; i++) {

            if (!isNamed) {
                name = pickName();
            }
            if (!hasLastName) {
                lastName = pickLastName();
            }
            if (!hasGender) {
                gender = picksex();
            }
            if (!hasRace) {
                race = pickRace();
            }
            if (!hasBackground) {
                background = pickBackground();
            }

            id = uuidGenerator.generatePeonId();

            peonslistToReturn.add(new Peon(id, name, lastName, gender, race, background,
                    attributesGenerator.generateAttributes(id, peonCreationRequestRepresentation.getEntropyLevel(),
                            List.of(peonCreationRequestRepresentation.getStrength(),
                                    peonCreationRequestRepresentation.getIntelligence(),
                                    peonCreationRequestRepresentation.getAgility(),
                                    peonCreationRequestRepresentation.getPerception(),
                                    peonCreationRequestRepresentation.getCharisma(),
                                    peonCreationRequestRepresentation.getLuck())).toString()));
        }

        return peonslistToReturn;
    }

    private String pickName() {
        return namesGenerator.generateName();
    }

    private String pickLastName() {
        return namesGenerator.generateLastName();
    }

    private String picksex() {
        if (random.nextBoolean()) {
            return "Female";
        } else {
            return "Male";
        }
    }

    private String pickRace() {
        int randomRaceIndex = random.nextInt(Races.values().length);
        int i = 0;
        for (Races race : Races.values()) {
            if (randomRaceIndex == i) {
                return race.toString();
            }
            i++;
        }
        return null;
    }

    private String pickBackground() {
        return "This is a perfectly generated background";
    }
}
