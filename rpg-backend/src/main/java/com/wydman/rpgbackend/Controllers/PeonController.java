package com.wydman.rpgbackend.Controllers;

import com.wydman.rpgbackend.Repository.Peon;
import com.wydman.rpgbackend.Domain.PeonFactory;
import com.wydman.rpgbackend.Service.PeonRequestDispatcher;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/peon")
public class PeonController {

    private PeonRequestDispatcher peonRequestDispatcher;

    public PeonController(PeonRequestDispatcher peonRequestDispatcher) {

        this.peonRequestDispatcher = peonRequestDispatcher;
    }

    @GetMapping("")
    public List<PeonRepresentation> getAllPeons() {
        List<PeonRepresentation> peonRepresentationList = new ArrayList<>();
        for (Peon peon : peonRequestDispatcher.getAllPeons()) {
            peonRepresentationList.add(new PeonRepresentation(
                    peon.getName(),
                    peon.getLastName(),
                    peon.getSex(),
                    peon.getRace(),
                    peon.getBackgroud(),
                    peon.getAttributes()));
        }
        return peonRepresentationList;
    }
    @PostMapping("/generator")
    public List<PeonRepresentation> createPeons(@RequestBody PeonCreationRequestRepresentation peonCreationRequestRepresentation) {
        return peonRequestDispatcher.requestCreatePeonstoFactory(peonCreationRequestRepresentation);
    }

}
