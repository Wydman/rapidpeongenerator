package com.wydman.rpgbackend.Controllers;

public class PeonRepresentation {
    private String name;
    private String lastName;
    private String sex;
    private String race;
    private String background;
    private String attributes;

    public PeonRepresentation(String name, String lastName, String sex, String race, String background, String attributes) {
        this.race = race;
        this.background = background;
        this.name = name;
        this.lastName = lastName;
        this.sex = sex;
        this.attributes = attributes;
    }

    public PeonRepresentation() {
    }


    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }
}
