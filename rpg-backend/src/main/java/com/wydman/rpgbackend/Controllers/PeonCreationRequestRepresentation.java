package com.wydman.rpgbackend.Controllers;

public class PeonCreationRequestRepresentation {

    private int strength;
    private int intelligence;
    private int agility;
    private int perception;
    private int charisma;
    private int luck;
    private int numberOfPeonToCreate;
    private int entropyLevel;
    private String name;
    private String lastName;
    private String gender;
    private String race;
    private String background;

    public PeonCreationRequestRepresentation(int strength,
                                             int intelligence,
                                             int agility,
                                             int perception,
                                             int charisma,
                                             int luck,
                                             int numberOfPeonToCreate,
                                             int entropyLevel,
                                             String name,
                                             String lastName,
                                             String gender,
                                             String race,
                                             String background) {
        this.strength = strength;
        this.intelligence = intelligence;
        this.agility = agility;
        this.perception = perception;
        this.charisma = charisma;
        this.luck = luck;
        this.numberOfPeonToCreate = numberOfPeonToCreate;
        this.entropyLevel = entropyLevel;
        this.name = name;
        this.lastName = lastName;
        this.gender = gender;
        this.race = race;
        this.background = background;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getPerception() {
        return perception;
    }

    public void setPerception(int perception) {
        this.perception = perception;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

    public int getNumberOfPeonToCreate() {
        return numberOfPeonToCreate;
    }

    public void setNumberOfPeonToCreate(int numberOfPeonToCreate) {
        this.numberOfPeonToCreate = numberOfPeonToCreate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return gender;
    }

    public void setSex(String gender) {
        this.gender = gender;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public int getEntropyLevel() {
        return entropyLevel;
    }

    public void setEntropyLevel(int entropyLevel) {
        this.entropyLevel = entropyLevel;
    }
}
