package com.wydman.rpgbackend.Service;


import com.wydman.rpgbackend.Domain.AttributesGenerator;
import com.wydman.rpgbackend.Domain.UuidGenerator;
import com.wydman.rpgbackend.Repository.Peon;
import com.wydman.rpgbackend.Repository.PeonRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RapidPeonGeneratorCore {

    private final PeonRepository peonRepository;
    private final UuidGenerator uuidGenerator;
    private AttributesGenerator attributesGenerator;

    RapidPeonGeneratorCore(PeonRepository peonRepository, UuidGenerator uuidGenerator, AttributesGenerator attributesGenerator) {

        this.peonRepository = peonRepository;
        this.uuidGenerator = uuidGenerator;
        this.attributesGenerator = attributesGenerator;
    }

    public void start() {
        for (int i = 0; i <= 100; i++) {
            String newId=uuidGenerator.generatePeonId();
            Peon peon = new Peon(newId, "youpi" + i, "Banane", "male", "", "", attributesGenerator.generateAttributes(newId,100,List.of(0, 0, 0, 0, 0, 0)).toString());
            peonRepository.save(peon);
        }
    }

    public void deleteAll() {
        peonRepository.deleteAll();
    }
}
