package com.wydman.rpgbackend.Service;

import com.wydman.rpgbackend.Controllers.PeonCreationRequestRepresentation;
import com.wydman.rpgbackend.Controllers.PeonRepresentation;
import com.wydman.rpgbackend.Domain.PeonFactory;
import com.wydman.rpgbackend.Domain.PeonRepresentationFactory;
import com.wydman.rpgbackend.Repository.Peon;
import com.wydman.rpgbackend.Repository.PeonRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PeonRequestDispatcher {

    private final PeonFactory peonFactory;
    private final PeonRepository peonRepository;
    private PeonRepresentationFactory peonRepresentationFactory;

    public PeonRequestDispatcher(PeonFactory peonFactory, PeonRepository peonRepository, PeonRepresentationFactory peonRepresentationFactory) {

        this.peonFactory = peonFactory;
        this.peonRepository = peonRepository;
        this.peonRepresentationFactory = peonRepresentationFactory;
    }

    public Iterable<Peon> getAllPeons() {
        Iterable<Peon> peonList = new ArrayList<>();
        return peonRepository.findAll();
    }

    public List<PeonRepresentation> requestCreatePeonstoFactory(PeonCreationRequestRepresentation peonCreationRequestRepresentation) {
        List<Peon> peonList;
        List<PeonRepresentation> peonListToReturn;

        peonList = peonFactory.CreatePeon(peonCreationRequestRepresentation);
        peonListToReturn = peonRepresentationFactory.getPeonRepresentationsFromPeons(peonList);


        System.out.println(peonCreationRequestRepresentation.getSex());
        System.out.println(peonCreationRequestRepresentation.getAgility());
        System.out.println(peonCreationRequestRepresentation.getBackground());
        System.out.println(peonCreationRequestRepresentation.getIntelligence());
        System.out.println(peonCreationRequestRepresentation.getStrength());
        System.out.println(peonCreationRequestRepresentation.getIntelligence());
        System.out.println(peonCreationRequestRepresentation.getNumberOfPeonToCreate());
        System.out.println(peonCreationRequestRepresentation.getPerception());
        System.out.println(peonCreationRequestRepresentation.getRace());
        System.out.println(peonList);
        System.out.println(peonListToReturn);
        peonRepository.saveAll(peonList);
        return peonListToReturn;
    }
}
